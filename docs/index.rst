.. Django Scoped RBAC documentation master file, created by
   sphinx-quickstart on Sun Dec 29 09:02:00 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django Scoped RBAC's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   context
   contexts_collection
   role
   roles_collection
   role_assignment
   role_assignments_collection
   resource_envelope.rst
   paging.rst
   
.. todo:: Figure out how to add api doc...

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
