from django.apps import AppConfig


class ScopedRbacConfig(AppConfig):
    name = "scoped_rbac"
